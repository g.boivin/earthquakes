import moment from 'moment';

import { SEARCH_URL, DATE_FORMAT } from './constants';

export function search(query) {
  return fetch(`${SEARCH_URL}${query.toUrlParams()}`).then((response) => response.json());
}

/**
 * Represents a query to the earthquake API at
 * https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson
 */
export class Query {
  params = {
    starttime: moment().subtract(50, 'years').format(DATE_FORMAT),
    endtime: moment().format(DATE_FORMAT),
    maxradiuskm: 500,
    minmag: 6,
    limit: 1000,
    orderby: 'magnitude',
  };

  constructor(params) {
    this.params = {
      ...this.params,
      ...params,
    };
  }

  toUrlParams() {
    return Object.entries(this.params).map(([key, val]) => `${key}=${val}`).join('&');
  }
}