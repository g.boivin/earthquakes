import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import EditableField from '../EditableField';
import { MAGNITUDES, DISTANCES, YEARS } from '../../constants';

class InteractiveSearch extends Component {
  editableField(field, value, children) {
    const { query, updateQuery } = this.props;
    return <EditableField
      query={query}
      updateQuery={updateQuery}
      field={field}
      value={value}>
      {children}
    </EditableField>
  }
  render () {
    const { query, earthquakes = [] } = this.props;
    const singular = earthquakes.length === 1;
    return (
      <div className="answer-wrapper">
        <span>
          There {singular ? 'was' : 'were'} {earthquakes.length ? earthquakes.length : 'no'}
        </span>
        {' '}
        <span>
          {this.editableField(
            'minmag',
            `${MAGNITUDES[query['minmag']]}`,
            <small>(magnitude {query['minmag']} or above)</small>
          )}
        </span> 
        <span> earthquake{singular ? '' : 's'} recorded in a </span>
        <span>
        {this.editableField(
          'maxradiuskm',
          query['maxradiuskm'],
          <small>{DISTANCES[query['maxradiuskm']]}</small>
        )} km radius
        </span>
        <span> in the past{' '}
        {this.editableField(
          'starttime',
          moment().diff(query.starttime, 'years'),
          <small>{YEARS[moment().diff(query.starttime, 'years')]}</small>
        )} years
        </span>
      </div>
    );
  }
}

InteractiveSearch.propTypes = {
  query: PropTypes.shape({
    starttime: PropTypes.string.isRequired,
    endtime: PropTypes.string.isRequired,
    maxradiuskm: PropTypes.number.isRequired,
    latitude: PropTypes.number.isRequired,
    longitude: PropTypes.number.isRequired,
    limit: PropTypes.number,
  })
};

export default InteractiveSearch;
