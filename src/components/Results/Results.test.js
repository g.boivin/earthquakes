import React from 'react';
import { shallow } from 'enzyme';
import Results from './';

describe('Results', () => {
  it('should render correctly with correct props', () => {
    const props = {
      coords: {
        latitude: 22.323,
        longitude: 24.1412,
      },
    }
    const component = shallow(<Results {...props} />);
  
    expect(component).toMatchSnapshot();
  });
});