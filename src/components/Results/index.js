import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import { search, Query } from '../../Api'; 
import InteractiveSearch from '../InteractiveSearch';
import Loading from '../Loading';
import Earthquake from '../Earthquake';
import { CITIES} from '../../constants';

import './Results.css';

export default class Results extends Component {
  constructor() {
    super();
    this.state = {};
  }
  componentDidMount() {
    const { coords }  = this.props;

    const query = new Query({
      longitude: coords.longitude,
      latitude: coords.latitude,
    });

    this.setState({
      query: query.params,
      searching: true,
    }, () => {
      this.search()
    });
  }

  componentWillReceiveProps(newProps) {
    const { latitude, longitude } = this.props.coords;
    const { latitude: newLatitude, longitude: newLongitude } = newProps.coords;
    if (newLatitude !== latitude || newLongitude !== longitude) {
      const query = new Query({
        longitude: newLongitude,
        latitude: newLatitude,
      });

      this.setState({
          query: query.params,
          searching: true,
          answer: null,
        }, () => {
          this.search()
      });
    }
  }

  search = () =>{
    return search(new Query(this.state.query))
      .then(json => this.setState({
        earthquakes: json.features,
        searching: false,
        answer: this.state.answer ? this.state.answer : (json.features.length > 0 ? 'Yes' : 'No')
      }));
  }

  updateQuery = (update) => {
    this.setState({
      query: {
        ...this.state.query,
        ...update,
      },
      searching: true,
    }, this.search) 
  }

  renderFooter() {
    const { setCity } = this.props;
    return (
      <footer className="cities"> What about in these cities?
      {CITIES.map(city => <span key={city.name} onClick={() => {
        setCity(city);
      }
      }>{city.name}</span>)}
      </footer>
    );
  }

  render() {
    const { searching, query, earthquakes, answer } = this.state;
    const { coords } = this.props;
    return (
      <div className='results'>
        {answer ? <p className="answer">{answer}.</p> : null}
        {searching ? <Loading /> : <Fragment>
          <div>
            {query && <InteractiveSearch updateQuery={this.updateQuery} query={query} earthquakes={earthquakes}/>}
          </div>
          <div className="earthquake-list">
            {earthquakes && earthquakes.map(e => <Earthquake  key={e.id} coords={coords} {...e} />)}
          </div>
        </Fragment>}
        {this.renderFooter()}
      </div>
    )
  }
}

Results.propTypes = {
  coords: PropTypes.shape({
    latitude: PropTypes.number,
    longitude: PropTypes.number,
  })
}