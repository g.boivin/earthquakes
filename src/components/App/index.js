import React, { Component } from 'react';
import './App.css';
import Results from '../Results';
import Loading from '../Loading';

class App extends Component {
  constructor() {
    super();
    this.state = {};
  }

  getGeoLocation = () => {
    if (navigator.geolocation && !this.state.location) {
      navigator.geolocation.getCurrentPosition(this.setGeoLocation, this.handleRejection);
      this.setState({
        searching: true,
      });
    }
  }

  setGeoLocation = (location) => {
    this.setState({
      location: location,
      searching: false,
    });
  }

  setCity = (city) => {
    this.setState({
      city: city.name,
      location: {
        coords: city.coords,
      },
    })
  }

  handleRejection = (error) => {
    this.setState({
      error: error.code,
      searching: false,
    });
  }

  renderQuestion() {
    const { city } = this.state;
    return <h1
      className={`question ${!this.state.location && 'shake-slow'}`}
      onClick={this.getGeoLocation}
    >
      Should {city ? city : 'I'} worry about earthquakes?
    </h1>;
  }

  renderSubText() {
    const { error, searching } = this.state;
    const errors = {
      //User denied permission
      1: 'You have blocked us from figuring out your location :(',
      //Position unavailable
      2: 'Your position is unavailable :(',
      //Geolocation call timed out
      3: 'We did not manage to get your position in a timely manner.'
    };

    if (error) {
      return <p>{errors[this.state.error]}</p>;
    } else if (searching) {
      return <div>Figuring out your location<Loading/></div>;
    }

    return null;
  }

  renderResults() {
    const { location } = this.state;
    return location && location.coords && <Results coords={location.coords} setCity={this.setCity}/>
  }

  render() {    
    const { location } = this.state;

    return (
      <div className={`App ${!location ? 'intro' : ''}`}>
        {this.renderQuestion()}
        {this.renderSubText()}
        {this.renderResults()}
      </div>
    );
  }
}

export default App;
