import React, { Component, Fragment } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

import { MAGNITUDES, DATE_FORMAT } from '../../constants';
import './EditableField.css';

class EditableField extends Component {
  constructor() {
    super();
    this.state = {
      visible: false,
    };
  }
  toggle = (visible) => {
    this.setState({
      visible: visible !== undefined ? visible : !this.state.visible,
    });
  }
  onChange = ( { target } ) => {
    this.toggle(false);
    const update = {[target.name]: isNaN(target.value) ? target.value : parseInt(target.value)};
    this.props.updateQuery(update);
  }

  getEditViewForField() {
    const { field, query } = this.props;
    
    switch(field) {
      case 'minmag': 
        return <select name={field} value={query[field]} onChange={this.onChange}>
          {Object.keys(MAGNITUDES).reverse().map(key => <option key={key} value={key}>{MAGNITUDES[key]}</option>)}
        </select>
      
      case 'maxradiuskm':
        const options = [10, 50, 500, 5000, 20000];
        return <select name={field} value={query[field]} onChange={this.onChange}>
          {
            options.map(radius => <option key={radius} value={radius}>{radius}</option>)
          }
        </select>
      
      case 'starttime': 
        const yearOptions = [1, 5, 10, 50, 100];
        return <select name={field} value={query[field]} onChange={this.onChange}>
          {
            yearOptions.map(years => <option key={years} value={moment().subtract(years, 'years').format(DATE_FORMAT)}>{years}</option>)
          }
      </select>
      
      default:
        return null;
    }
  }
  render() {
    const { field, value, children } = this.props;
    return(
      <Fragment>
        {
          this.state.visible ? 
          this.getEditViewForField()
          : <span className='editable-value' data-field={field} onClick={this.toggle}>{value}{children}</span>
        }
        </Fragment>
    );
  }
};

EditableField.propTypes = {
  query: PropTypes.shape({
    maxradiuskm: PropTypes.number.isRequired,
    latitude: PropTypes.number.isRequired,
    longitude: PropTypes.number.isRequired,
    starttime: PropTypes.string.isRequired,
    endtime: PropTypes.string.isRequired,
  }).isRequired,
  field: PropTypes.string.isRequired,
  updateQuery: PropTypes.func.isRequired,
  children: PropTypes.element.isRequired,
};

export default EditableField;