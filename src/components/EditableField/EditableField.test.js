import React from 'react';
import { shallow } from 'enzyme';
import EditableField from './';


describe('EditableField', () => {
  it('should render correctly with correct props', () => {
    const query = {
      starttime: '1969-07-16',
      endtime: '1969-07-16',
      maxradiuskm: 45,
      latitude: 54.432,
      longitude: 43.432,
      minmag: 4,
    }
    const updateQuery = () => {}

    const component = shallow(<EditableField
      query={query}
      updateQuery={updateQuery}
      field="minmag"
      value={query["minmag"]}>
        <small>indication on current value</small>
      </EditableField>);
  
    expect(component).toMatchSnapshot();
  });
});