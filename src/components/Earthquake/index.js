import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

import { getRoundedDistance } from '../../distance';
import './Earthquake.css';


class Earthquake extends Component {
  openEarthquakeLink = () => {
    window.open(this.props.properties.url, '_blank');
  }
  render() {
    const { properties, geometry, coords } = this.props;
    const { time, mag, place } = properties;
    return <div className="earthquake" onClick={this.openEarthquakeLink}>
      <span data-mag={Math.floor(mag)} className="mag">{mag}</span>
      <span className="years">{moment(time).fromNow()}</span>
      <span className="distance">{getRoundedDistance(coords, {latitude: geometry.coordinates[1], longitude: geometry.coordinates[0]})} km away</span>
      <span className="place">{place}</span>
    </div>
  }
};

Earthquake.propTypes = {
  properties: PropTypes.shape({
    time: PropTypes.number.isRequired,
    mag: PropTypes.number.isRequired,
    place: PropTypes.string.isRequired,
  }).isRequired,
  geometry: PropTypes.shape({
    coordinates: PropTypes.array.isRequired,
  }).isRequired,
  coords: PropTypes.shape({
    latitude: PropTypes.number.isRequired,
    longitude: PropTypes.number.isRequired,
  }),
};

export default Earthquake