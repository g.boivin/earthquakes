import React from 'react';
import { shallow } from 'enzyme';
import Earthquake from './';

describe('Earthquake', () => {
  it('should render correctly with correct props', () => {
    const earthQuake = {
      properties: {
        time: new Date().getTime(),
        mag: 4.2,
        place: 'Near my house :o',
      },
      geometry: {
        coordinates: [54.49243, 42.414],
      },
      coords: {
        latitude: 22.323,
        longitude: 24.1412,
      },
    }
    const component = shallow(<Earthquake {...earthQuake} />);
  
    expect(component).toMatchSnapshot();
  });
});