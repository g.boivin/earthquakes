import React from 'react';

import './Loading.css';

export default function Loading() {
  return <div className="loading shake-slow shake-constant">…</div>
};
