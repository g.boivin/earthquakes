export const DATE_FORMAT = 'YYYY-MM-DD';

export const SEARCH_URL = 'https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&';

export const MAGNITUDES = {
  1: 'micro',
  2: 'minor',
  4: 'light',
  5: 'moderate',
  6: 'strong',
  7: 'major',
  8: 'great',
};

export const DISTANCES = {
  10: 'A bit smaller than Berlin',
  50: 'Berlin to Poland',
  500: 'Berlin to Munich',
  5000: 'Berlin to Dakar, Senegal',
  20000: 'Anywhere',
};

export const YEARS = {
  100: '1919 - Invention of the pop-up toaster',
  50: '1969 - Invention of the Internet',
  10: '2009 - Obama becomes President',
  5: '2014 - True detective debuts',
  1: "2018 - Gru's plan meme",
};

export const CITIES = [
  {name: 'San Francisco', coords: { latitude: 37.774929, longitude: -122.419416}},
  {name: 'Tokyo', coords: { latitude: 35.689487, longitude: 139.691706}},
  {name: 'Saõ Paulo', coords: { latitude: -23.55052, longitude: -46.633309}},
  {name: 'Sydney', coords: { latitude: -33.8688, longitude: 151.2093}},
];