import { Query } from './Api';

describe('Api', () => {
  it('toUrlParams works as expected', () => {
    const params = {
      longitude: 32,
      latitude: 41,
      starttime: '1969-07-16',
      endtime: '1969-07-16',
      maxradiuskm: 500,
      minmag: 6,
      limit: 1000,
      orderby: 'magnitude',
    };
    expect(new Query(params).toUrlParams()).toEqual("starttime=1969-07-16&endtime=1969-07-16&maxradiuskm=500&minmag=6&limit=1000&orderby=magnitude&longitude=32&latitude=41");
  });
});