## Should you be worried about earthquakes? 

Find out by running `yarn install && yarn start`

## Gitlab pages version

The application is also accessible at: [https://g.boivin.gitlab.io/earthquakes/]( https://g.boivin.gitlab.io/earthquakes/)

Please ignore the ssl warning due to the lack of certificate.